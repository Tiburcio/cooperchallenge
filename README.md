# cooperChallenge

Projeto Cooper em react native.

## Installation

1) Para executar o projeto basta ter o Node, Java e Android Studio instalados e configurados, bem como, a versão 8 do JDK. Com as variáveis de ambiente java e android configurados;

2) Para executar o projeto em localhost basta executar o seguinte comando:

   npm install ou yarn install

3) Apos instalar as dependências do projeto basta executar o comando do react native para executar o projeto no emulador do android:

   npx react-native run-android