import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../../../styles';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1.4,
    justifyContent: 'center',
    padding: metrics.basePadding,
    borderColor: '#e6e6e6',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    fontSize: 16,
    fontWeight: "bold",
    color: `#000000`,
  }
});

export default styles;
