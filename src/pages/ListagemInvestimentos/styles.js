import { StyleSheet } from 'react-native';
import { colors } from '../../styles';

const styles = StyleSheet.create({
  linYellow: {
    backgroundColor: colors.warning,
    height: 4,
    width: '100%',
  },
});

export default styles;