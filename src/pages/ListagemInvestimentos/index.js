import React, { useEffect, useState } from 'react';
import {View, FlatList, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import api from '../../services/api';
import InvestimentoItem from './Components/InvestimentoItem';
import Item from '../../components/Item';
import styles from './styles';

const ListagemInvestimentos = () => {
  const [investimentos, setInvestimentos] = useState([]);

  const navigation = useNavigation();

  useEffect(() => {
    const listarInvestimentos = async () => {
      try {
        const { data } = await api.get('7b2dfe42-37a3-4094-b7ce-8ee4f8012f30');
        setInvestimentos(data.response.data.listaInvestimentos);
      } catch (error) {
      }
    };

    listarInvestimentos();
  }, []);

  const renderItem = (item) => (
    <InvestimentoItem
      onPress={() => navigation.navigate('Resgate', { investimento: item })}
      investimento={item}
      />
  );

  const renderList = () => (
    <FlatList
      data={investimentos}
      renderItem={({ item }) => renderItem(item)}
      keyExtractor={(item) => item.id}
    />
  );

  return (
    <View>
      <View style={styles.linYellow} />
      <Item label="INVESTIMENTOS" value="R$"/>
      {renderList()}
    </View>
  );
};

export default ListagemInvestimentos;
