import axios from 'axios';

const api = axios.create({
  baseURL: 'http://www.mocky.io/v3/',
});
export default api;
