import { StyleSheet } from 'react-native';
import { colors, metrics } from '../../styles';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 24,
  },

  text: {
    fontSize: 16,
    fontWeight: '600',
    color: '#595959'
  },

  titleText: {
    fontSize: 16,
    fontWeight: "bold",
    color: `#000000`,
  }
});

export default styles;