import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { colors } from './styles';

import ListagemInvestimentos from './pages/ListagemInvestimentos';
import DetalharInvestimentos from './pages/DetalharInvestimentos';

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="ListagemInvestimentos"
          options={{
            title: 'Resgate',
            headerTitleStyle: { alignSelf: 'center', color: colors.white },
            headerStyle: {
              backgroundColor: '#4169e1',
            },
          }}
          component={ListagemInvestimentos}
        />
        <Stack.Screen
          name="Resgate"
          options={{
            title: 'Resgate',
            headerTintColor: `#ffffff`,
            headerTitleStyle: { alignSelf: 'center', color: colors.white },
            headerStyle: {
              backgroundColor: '#4169e1',
            },
          }}
          component={DetalharInvestimentos}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
